<?php
require_once("./src/conexion.php");
session_start();
if (!isset($_SESSION['usuario'])){
    header("Location: index.php");
} 
$usuario = $_SESSION['usuario'];


$sql = "SELECT * from usuarios where usuario = '$usuario'";
$resultado = $conexion->query($sql);
$row = $resultado->fetch_assoc();   
?>

<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./css/styles.css">
    <link href="https://fonts.googleapis.com/css2?family=Archivo:wght@200&family=Signika+Negative&display=swap" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://rawgit.com/eKoopmans/html2pdf/master/dist/html2pdf.bundle.min.js"></script>

    <title>Turnos</title>
    <style>
        .navegacion a {
            text-decoration: none;
        }
        
        table {
            height: 10rem;
            background-color: rgb(211, 208, 208);
            border-collapse: collapse;
            font-size: 12pt;
            font-family: arial;
            width: 100%;
                
        }
                
        table th {
            text-align: center;
            padding: 10;
            background: #3d7ba8;
            color: #fff;
            height: 3rem
        }

        table tr:nth-child(odd) {
            background: #fff;
        }

        table td{
            text-align: center;
            padding: 10px;
        }
    </style>
    
</head>

<body>
           
    <header class="site-header">
        <div class="contenedor contenido-header">
            <div class="barra">
                <a href="lista.php">
                    <img src="./img/proyectoLogo.png" alt="logo proyecto">
                </a>


                <nav id="navegacion" class="navegacion">
                    <a style="color: white;">Bienvenido <?php echo utf8_decode($row['usuario']); ?></a> 

                    <a href="index.php">Inicio</a>
                    <a href="./src/salirsesion.php" onclick="return ConfirmSession()">Cerrar Sesión</a>


                </nav>
            </div>
        </div>
    </header>

    <h2 style="text-align:center; margin-top:3rem;">Historial de Turnos</h2>
    <br>
    <input class= "btn btn-info" id="imprimir" type="submit" value="Informe">
     

    <section id="tabla">
        <br>
        <table>


            <tr>
                <th>Nombre</th>
                <th>Area</th>
                <th>Trámite</th>
                <th>Observacion</th>
    
            </tr>
            <?php
                
                $sql="SELECT * from turno";
                $result=mysqli_query($conexion,$sql);
                while($mostrar=mysqli_fetch_array($result)){
            ?>
            <tr>
                
                <td><?php echo $mostrar['nombre']?></td>
   
                <td><?php echo $mostrar['area']?></td>
                <td><?php echo $mostrar['tramite']?></td>
                <td><?php echo $mostrar['observacion']?></td>
                
            </tr>

            <?php
            }

            ?>
        </table>
    </section>

    <br>
    <br>
    <br>

    <footer class="site-footer seccion">
        <div class="contenedor contenedor-footer">
            <nav class="navegacion">
                <a href="#">¿Quienes somos?</a>
                <a href="#">Terminos y Condiciones</a>
                <a href="#">Contacto</a>
            </nav>
            <p class="copyright">Todos los Derechos Reservados 2022 &copy; </p>
        </div>
    </footer>


<script src="./js/modal.js?v=<?= $rand ?>"></script>

<script type="text/javascript">

    function ConfirmSession() {
        var respuesta = confirm("¿Está seguro que desea cerrar sesión?");
        if (respuesta == true) {
            //header("location: index.php")
            return true;
        } else {
            return false;
        }
    }
</script>

</body>

</html>
